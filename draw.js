var COLORS = [
    'rgb(0, 0, 255)',
    'rgb(0, 255, 0)',
    'rgb(0, 255, 255)',
    'rgb(255, 0, 0)',
    'rgb(255, 0, 255)',
    'rgb(255, 255, 0)',
];
/* 点の描画
* x: 点の座標配列
* y: 点のラベル
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoint(x, canvas, r, color) {
    ctx = canvas.getContext('2d');
    var c = dataToCanvas(x, canvas, r);

    ctx.beginPath();
    ctx.strokeStyle = color;
    ctx.arc(c[0], c[1], canvas.width / 125, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 点群の描画
* X: 点群の座標配列
* Y: 点群のラベル配列
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoints(X, canvas, r, m) {
    for (var i=0; i < X.length; ++i) {
        var x = X[i];
        var color;
        if (m !== undefined) {
            var cluster = m.judge(x);
            color = COLORS[cluster % COLORS.length];
        } else {
            color = 'rgb(0, 0, 0)';
        }
        drawPoint(x, canvas, r, color);
    }
    if (m === undefined) {
        return;
    }
    for (var i=0; i < m.centroids.length; ++i) {
        var centroid = m.centroids[i];
        var color = COLORS[i % COLORS.length];
        for (var j=0; j < m.clusterIndices[i].length; ++j) {
            drawLine(centroid, X[m.clusterIndices[i][j]], canvas, r);
        }
        drawCross(centroid, canvas, r, color);
    }
}

function drawLine(x1, x2, canvas, r) {
    var ctx = canvas.getContext('2d');

    var c1 = dataToCanvas(x1, canvas, r);
    var c2 = dataToCanvas(x2, canvas, r);

    ctx.beginPath();
    ctx.moveTo(c1[0], c1[1]);
    ctx.lineTo(c2[0], c2[1]);
    ctx.stroke();
}

/* 座標系の変換 */
function dataToCanvas(x, canvas, r) {
    w = canvas.width * (1 / 2 + x[0] / r[0]);
    h = canvas.height * (1 / 2 - x[1] / r[1]);
    return [w, h];
}

function canvasToData(c, canvas, r) {
    x = r[0] * (c[0] / canvas.width - 1 / 2);
    y = r[1] * (-c[1] / canvas.height + 1 / 2);
    return [x, y];
}

function drawCross(x, canvas, r, color) {
    var cx = dataToCanvas(x, canvas, r);
    var w = canvas.width / 125;
    /* バツ印の左上座標 */
    var cx1 = cx[0] - w;
    var cy1 = cx[1] - w;
    /* バツ印の右下座標 */
    var cx2 = cx[0] + w;
    var cy2 = cx[1] + w;
    /* バツ印の右上座標 */
    var cx3 = cx[0] + w;
    var cy3 = cx[1] - w;
    /* バツ印の左下座標 */
    var cx4 = cx[0] - w;
    var cy4 = cx[1] + w;

    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.moveTo(cx1, cy1);
    ctx.lineTo(cx2, cy2);
    ctx.moveTo(cx3, cy3);
    ctx.lineTo(cx4, cy4);
    ctx.strokeStyle = color;
    ctx.lineWidth = 2;
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    ctx.lineWidth = 1;
}

/* 座標軸の描画
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawAxes(canvas, r) {
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    /* x軸の描画 */
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.moveTo(canvas.width, canvas.height / 2);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 53 / 110);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 57 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 105 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 105 / 110, canvas.height * 56 / 110);
    ctx.moveTo(canvas.width * 5 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 5 / 110, canvas.height * 56 / 110);

    /* y軸の描画 */
    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, 0);
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width * 53 / 110, canvas.height * 2 / 110);
    ctx.lineTo(canvas.width * 57 / 110, canvas.height * 2 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 5 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 5 / 110);
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 105 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 105 / 110);

    ctx.stroke();
    ctx.fill();

    /* 文字の描画 */
    var normalSize = Math.min(canvas.width, canvas.height) / 20;
    var fontname = String(normalSize) + "px 'Times New Roman'";
    var fontnameItalic = "Italic " + fontname

    ctx.font = fontnameItalic;
    ctx.textAlign = 'left';
    ctx.textBaseline = 'middle';
    ctx.fillText('y', canvas.width * 58 / 110, canvas.height * 2 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'middle';
    ctx.font = fontname;
    ctx.fillText(String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 5 / 110);
    ctx.fillText('-' + String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 105 / 110);

    ctx.font = fontnameItalic;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.fillText('x', canvas.width * 108 / 110, canvas.height * 52 / 110);

    ctx.font = fontname;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillText(String(r[1] / 2), canvas.width * 105 / 110, canvas.height * 58 / 110);
    ctx.fillText('-' + String(r[1] / 2), canvas.width * 5 / 110, canvas.height * 58 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'top';
    ctx.fillText('0', canvas.width * 54 / 110, canvas.height * 56 / 110);
}
