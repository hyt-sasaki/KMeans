var KMeans = (function () {
    /* コンストラクタ */
    var KMeans = function (m, i) {
        this.model = m; //学習モデル
        this.itr = i;   //学習回数
    }

    var pt = KMeans.prototype;

    /* 学習 */
    pt.train = function (Data) {
        var itrCount = 0;
        do {
            var oldClusterIndices = this.model.centroids.slice();
            this.update(Data);
            itrCount++;
            if (itrCount >= this.itr) {
                break;
            }
            if (this.check(oldClusterIndices)) {
                if (itrCount !== 1) {
                    break;
                }
            }
        } while(true);
    }

    pt.update = function (Data) {
        this.calcCentroids(Data);
        this.assignCluster(Data);
    }

    pt.check = function (oldClusterIndices) {
        for (var i=0; i < oldClusterIndices.length; ++i) {
            var flg = false;
            for (var j=0; j < this.model.centroids.length; ++j) {
                if (oldClusterIndices[i] === this.model.centroids[j]) {
                    flg = true;
                    break;
                }
            }
            if (!flg) {
                return false;
            }
        }
        return true;
    }

    pt.assignCluster = function (Data) {
        for (var i=0; i < this.model.clusterIndices.length; ++i) {
            this.model.clusterIndices[i] = [];
        }
        for (var i=0; i < Data.length; ++i) {
            var cluster = this.model.judge(Data[i]);
            this.model.clusterIndices[cluster].push(i);
        }
    }

    pt.calcCentroid = function (Data, n) {
        if (this.model.clusterIndices[n].length === 0) {
            return this.model.centroids[n];
        }
        var d = Data[this.model.clusterIndices[n][0]];
        if (this.model.clusterIndices[n].length === 1) {
            return d;
        }
        for (var i=1; i < this.model.clusterIndices[n].length; ++i) {
            d = vectorAddition(d, Data[this.model.clusterIndices[n][i]]);
        }

        d = vectorDevision(d, this.model.clusterIndices[n].length);

        return d;
    }

    pt.calcCentroids = function (Data) {
        for (var i=0; i < this.model.centroids.length; ++i) {
            this.model.centroids[i] = this.calcCentroid(Data, i);
        }
    }

    return KMeans;
})();

function vectorAddition(d1, d2) {
    var ret = [];
    for (var i=0; i < d1.length; ++i) {
        ret[i] = d1[i] + d2[i];
    }

    return ret;
}

function vectorDevision(d, n) {
    var ret = [];
    for (var i=0; i < d.length; ++i) {
        ret[i] = d[i] / n;
    }

    return ret;
}
