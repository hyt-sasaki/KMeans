/* 学習モデル */
var Model = (function() {
    /* コンストラクタ */
    var Model = function(c) {
        this.centroids = c; //初期セントロイド集合
        this.clusterIndices = [];
        for (var i=0; i < this.centroids.length; ++i) {
            this.clusterIndices[i] = [];
        }
    }

    var pt = Model.prototype;

    pt.judge = function (d) {
        var minDist = Number.MAX_VALUE;
        var minDistIdx = -1;
        for (var i=0; i < this.centroids.length; ++i) {
            var dist = l2distnce(d, this.centroids[i]);
            if (dist < minDist) {
                minDist = dist;
                minDistIdx = i;
            }
        }

        return minDistIdx;
    }

    return Model;
})();

function l2distnce(d1, d2) {
    var ret = 0;
    for (var i=0; i < d1.length; ++i) {
        ret += Math.pow(d1[i] - d2[i], 2);
    }
    ret = Math.sqrt(ret);

    return ret;
}
