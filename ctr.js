$(function () {
    /* 訓練データの設定 */
    var RANGE = [20, 20];   //座標系の定義域幅
    (function () {
        var axesCanvas = $('#axesCanvas')[0];
        drawAxes(axesCanvas, RANGE);
    })();

    var dataRef = {Data:null};              //訓練データ集合
    var centroidsRef = {Centroids:null};      //セントロイド集合
    var fileRef = {file:null};

    var dataCanvas = $('#dataCanvas')[0];

    /* イベントハンドラの設定 */
    $('#dataCanvas').on('click', {dataRef:dataRef, centroidsRef:centroidsRef, range:RANGE, colors:COLORS}, onCanvasClicked);
    $('#learn').on('click', {dataRef:dataRef, centroidsRef:centroidsRef, range:RANGE, canvas:dataCanvas}, onTraining);
    $('#allResetButton').on('click', {dataRef:dataRef, centroidsRef:centroidsRef, canvas:dataCanvas}, onResetAll);
    $('#centroidsResetButton').on('click', {dataRef:dataRef, centroidsRef:centroidsRef, range:RANGE, canvas:dataCanvas}, onResetCentroids);
    $('#genFromFile').on('click', {dataRef:dataRef, fileRef:fileRef, canvas:dataCanvas, range:RANGE}, onGenerateFromFile);
    $('#openFile').on('change', {fileRef:fileRef}, onFileOpen);
    $(window).on('keydown', onShiftkeyDown);
    $(window).on('keydown', onEnterkeyDown);
});

function onShiftkeyDown(event) {
    if (event.shiftKey) {
        if($('input[name="clickLabel"]:eq(0)').prop('checked')) {
            $('input[name="clickLabel"]:eq(1)').prop('checked', true);
        } else {
            $('input[name="clickLabel"]:eq(0)').prop('checked', true);
        }
    }
}

function onEnterkeyDown(event) {
    if (event.which === 13) {
        console.log('Enter');
        $('#learn').trigger('click');
    }
}

function onTraining(event) {
    alert('学習開始');
    var itr = Number($('#itr').val());

    var m_l = new Model(event.data.centroidsRef.Centroids);
    var l = new KMeans(m_l, itr);
    l.train(event.data.dataRef.Data);

    var canvas = event.data.canvas;
    var range = event.data.range;
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    drawPoints(event.data.dataRef.Data, canvas, range, m_l);
}

function onCanvasClicked(event) {
    var c = [];
    var canvasRect = event.target.getBoundingClientRect();
    c[0] = event.clientX - canvasRect.left;
    c[1] = event.clientY - canvasRect.top;
    var range = event.data.range;
    var colors = event.data.colors;
    var d = canvasToData(c, event.target, range);
    var type = ($('input[name="clickLabel"]:checked').val() === "training") ? true: false;
    if (type) {
        if (event.data.dataRef.Data === null) {
            event.data.dataRef.Data = [];
        }
        event.data.dataRef.Data.push(d);
        drawPoint(d, event.target, range, 'rgb(0, 0, 0)');
    } else {
        if (event.data.centroidsRef.Centroids === null) {
            event.data.centroidsRef.Centroids = [];
        }
        event.data.centroidsRef.Centroids.push(d);
        var color = colors[event.data.centroidsRef.Centroids.length % colors.length];
        drawCross(d, event.target, range, color);
    }
}

function onResetAll(event) {
    event.data.dataRef.Data = [];
    event.data.centroidsRef.Centroids = [];

    var canvas = event.data.canvas;
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
}

function onResetCentroids(event) {
    event.data.centroidsRef.Centroids = [];

    var canvas = event.data.canvas;
    var range = event.data.range;
    canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height);
    drawPoints(event.data.dataRef.Data, canvas, range);
}
